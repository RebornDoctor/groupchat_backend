import express from 'express';
import controllers from '../controllers/index.js';
import middlewares from "../middlewares/index.js";

const router = express.Router();

// API to create user
router.post('/', controllers.auth.signup);

// API to login
router.post('/login', controllers.auth.login);

// API to generate new accessToken
router.post('/token', controllers.auth.reValidateUser)

router.get('/info', middlewares.auth.authenticateToken, controllers.auth.getUserInfo)

// logout the user and invalidate his refresh token
router.delete('/logout', middlewares.auth.authenticateToken, controllers.auth.logout);

export default router