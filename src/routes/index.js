import express from 'express';
import authRoutes from "./auth.js";
import groupsRoutes from "./group.js";

const router = express.Router();

router.use('/users', authRoutes);
router.use('/groups', groupsRoutes);

export default router;