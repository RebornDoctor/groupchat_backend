import middlewares from "../middlewares/index.js";
import controllers from "../controllers/index.js";
import express from 'express';

const router = express.Router();

// create group
router.post('/', middlewares.auth.authenticateToken, controllers.group.createGroup);

// get all groups
router.get('/', middlewares.auth.authenticateToken, controllers.group.getGroups);

// get a group
router.get('/:id', middlewares.auth.authenticateToken, controllers.group.getGroup);

// update group
router.put('/:id', middlewares.auth.authenticateToken, controllers.group.updateGroup);

// delete group
router.delete('/:id', middlewares.auth.authenticateToken, controllers.group.deleteGroup);

// Join a group
router.post('/:id/join', middlewares.auth.authenticateToken, controllers.group.joinGroup);

// read group messages

router.get('/:groupId/messages', middlewares.auth.authenticateToken, controllers.group.getGroupMessages);



export default router;