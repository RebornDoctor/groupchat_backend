
// to be able to access env variables
import 'dotenv/config';
import express from "express";
import routes from "./routes/index.js";
import cors from "cors";
import { connectToDatabase } from './database/index.js';
import { attachSocketIO } from './sockets/index.js';

const PORT = process.env.PORT || 3500;

const app = express();

// connect to DB
await connectToDatabase();

// make sure we are using JSON for our API's
app.use(express.json());
app.use(cors());

// app routes
app.use('/', routes);

const expressServer = app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

attachSocketIO(expressServer);

// global error middleware handler
app.use((err, req, res, next) => {
    //logger.error(err.stack);
    res.status(err.statusCode || 500)
        .send({ msg: err.message });
});

