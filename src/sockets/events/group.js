export const onJoinGroup = (socket, io) => async (groupId) => {
    const userId = socket.user.id;
    try {
        // const group = await models.Group.findById(groupId);

        socket.join(groupId);
        io.in(groupId).emit('userJoined', { userId, email: socket.user.email });

        // Record the groupId on the socket instance
        if (!socket.groups) {
            socket.groups = [];
        }

        socket.groups.push(groupId);

    } catch (error) {
        socket.emit('error', 'Group not found or error joining group');
    }
};

export const onLeaveGroup = (socket, io) => async (groupId) => {
    const userId = socket.user.id;
    try {
        //  const group = await models.Group.findById(groupId);

        socket.leave(groupId);
        io.in(groupId).emit('userLeft', { userId, email: socket.user.email });

    } catch (error) {
        socket.emit('error', 'Group not found or error leaving group');
    }
};

export default { onJoinGroup, onLeaveGroup };
