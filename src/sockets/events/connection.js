import groupEvents from "./group.js";
import messagesEvents from "./messages.js";

const attachEvents = (io) => {
  io.on('connection', (socket) => {
    console.log('a user connected with ID:', socket.id);

    // Wrap the event handlers to pass the io and socket objects
    socket.on('joinGroup', (groupId) => groupEvents.onJoinGroup(socket, io)(groupId));
    socket.on('sendMessage', (data) => messagesEvents.onSendMessage(socket, io)(data));
    socket.on('typing', (data) => messagesEvents.onTyping(socket, io)(data));

    socket.on('disconnect', async () => {
      console.log('user disconnected with ID:', socket.id);
      // Assuming you store the groups a user is in on their socket instance
      if (socket.groups) {
        for (const groupId of socket.groups) {
          // Use the leave group event logic when the user disconnects
          await groupEvents.onLeaveGroup(socket, io)(groupId);
        }
      }
    });
  });
};

export default attachEvents;
