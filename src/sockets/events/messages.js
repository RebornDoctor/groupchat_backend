import models from "../../models/index.js";

export const onSendMessage = (socket, io) => async ({ message, groupId }) => {
    const userId = socket.user.id;
    const userEmail = socket.user.email;

    try {
        const group = await models.Group.findById(groupId).select('+messages');
        const newMessage = {
            text: message,
            user: userId,
        };
        group.messages.push(newMessage);
        await group.save();

        const LastMsg = group.messages[group.messages.length - 1];
        io.in(groupId).emit('newMessage', { text: LastMsg.text, createdAt: LastMsg.createdAt, user: userEmail, id: LastMsg._id, userId });
    } catch (error) {
        console.log(error)
        socket.emit('error', 'Error sending message');
    }
};

export const onTyping = (socket, io) => ({ groupId, isTyping }) => {
    const userId = socket.user.id;
    io.in(groupId).emit('userTyping', { userId, isTyping });
};

export default { onSendMessage, onTyping };
