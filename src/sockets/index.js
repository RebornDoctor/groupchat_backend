import { Server } from 'socket.io';
import middlewares from '../middlewares/index.js';
import attachEvents from './events/connection.js';

export function attachSocketIO(expressServer) {
    const io = new Server(expressServer, {
        cors: {
            origin: '*', // Update with your client-side domain
        }
    });

    // Socket IO Auth middleware Route
    io.use(middlewares.auth.authenticateIoToken);

    attachEvents(io);
}
