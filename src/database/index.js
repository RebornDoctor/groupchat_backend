import mongoose from "mongoose";

export async function connectToDatabase() {
    try {
        const user = process.env.DB_USER;
        const password = process.env.DB_PASS;

        const connectionString = `mongodb+srv://${user}:${password}@cluster0.hzvkcjo.mongodb.net/`;
        await mongoose.connect(connectionString, {
            serverSelectionTimeoutMS: 5000
        });
    } catch (e) {
        console.log(e);
    }
}