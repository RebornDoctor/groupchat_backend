import jwt from 'jsonwebtoken';

export const createAccessToken = (user) => {
    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "30m" });
    return accessToken;
}


export const createRefreshToken = (user) => {
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: "30d" });
    return refreshToken;
}


export default { createAccessToken, createRefreshToken }