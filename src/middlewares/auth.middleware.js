import jwt from 'jsonwebtoken';

// auth token middleware
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    // get rid of Bearer 
    const token = authHeader && authHeader.split(' ')[1];
    // if there is no token in the header return 401
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        // user have token but it's invalid
        if (err) return res.sendStatus(403)
        // add the decrypted user object into the client request
        req.user = user;
        next();
    })

}

const authenticateIoToken = (socket, next) => {
    try {
        const token = socket.handshake.auth.token;
        if (token) {
            jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
                if (err) throw new HttpError(403);
                socket.user = user;
                next();
            });
        } else {
            throw new HttpError(401);
        }
    }
    catch (e) {
        next(e)
    }
};

export default { authenticateToken, authenticateIoToken }