import { Schema, model } from "mongoose";

const refreshTokenSchema = new Schema({
    owner: { type: Schema.Types.ObjectId, ref: 'User' }
});

export const RefreshToken = model('RefreshToken', refreshTokenSchema);
