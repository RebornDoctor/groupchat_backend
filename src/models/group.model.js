import { Schema, model } from "mongoose";

const groupSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    members: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    messages: {
        type: [{
            text: { type: String },
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User',
            },
            createdAt: {
                type: Date,
                default: Date.now,
            },
        }],
        select: false
    }
}, {
    timestamps: true // This will add createdAt and updatedAt fields
});


export const Group = model('Group', groupSchema);
