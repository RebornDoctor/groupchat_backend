import { Schema, model } from "mongoose";

const userSchema = new Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, select: false },
    groups: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Group',
        },
    ],
}, {
    timestamps: true // This will add createdAt and updatedAt fields
});

export const User = model('User', userSchema);
