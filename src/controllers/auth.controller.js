import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import models from "../models/index.js";
import HttpError from "../error/HttpError.js";
import { errorHandler, withTransaction } from "../utils/index.js";
import services from "../services/index.js";

const signup = errorHandler(withTransaction(async function (req, res, session) {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = { email: req.body.email, password: hashedPassword };

    // check if user exist regardless
    const isUserExisted = await models.User.findOne({ email: req.body.email }).exec() !== null;

    if (isUserExisted)
        throw new HttpError(400, 'Sorry but this email is already used for another account!');

    const userDoc = models.User(user);
    const refreshTokenDoc = models.RefreshToken({ owner: userDoc.id });

    await userDoc.save({ session });
    await refreshTokenDoc.save({ session });


    res.status(201);
    return { email: req.body.email, password: req.body.password, id: userDoc._id };
}));

const login = errorHandler(withTransaction(async function (req, res, session) {

    const user = await models.User
        .findOne({ email: req.body.email })
        .select('+password')
        .exec();

    if (user == null) {
        throw new HttpError(400, 'Wrong username or password');
    }

    if (await bcrypt.compare(req.body.password, user.password)) {
        const isLoggedOut = !(await models.RefreshToken.findOne({ owner: user.id }).exec());
        if (isLoggedOut) {
            const refreshTokenDoc = models.RefreshToken({ owner: user.id });
            await refreshTokenDoc.save({ session });
        }

        const accessToken = services.auth.createAccessToken({ id: user.id, email: user.email });
        const refreshToken = services.auth.createRefreshToken({ id: user.id, email: user.email });

        return { accessToken, refreshToken, id: user.id, ...req.body };
    } else {
        throw new HttpError(400, 'Wrong username or password');
    }
}));

const logout = errorHandler(withTransaction(async function (req, res, session) {
    await models.RefreshToken.findOneAndDelete({ owner: req.user.id }, { session }).exec();
    res.status(204);
}));

const getUserInfo = errorHandler(withTransaction(async function (req, res, session) {
    return req.user;
}));

const reValidateUser = errorHandler(withTransaction(async function (req, res, session) {
    const refreshToken = req.body.token;
    if (refreshToken == null) throw new HttpError(401);

    const generatedAccessToken = await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
        if (err) throw new HttpError(403);

        const isLoggedOut = !(await models.RefreshToken.findOne({ owner: user.id }).exec());
        if (isLoggedOut) throw new HttpError(403);

        const { iat, exp, ...serializedUser } = user
        const newAccessToken = services.auth.createAccessToken(serializedUser);
        return newAccessToken;
    });
    return { accessToken: generatedAccessToken };
}));

export default {
    reValidateUser,
    logout,
    login,
    signup,
    getUserInfo,
}