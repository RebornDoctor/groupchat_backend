import models from "../models/index.js";
import HttpError from "../error/HttpError.js";
import { errorHandler, withTransaction } from "../utils/index.js";

// Create a group
const createGroup = errorHandler(withTransaction(async (req, res, session) => {
    const group = new models.Group({
        name: req.body.name,
        description: req.body.description,
        members: [req.user.id], // Add the creator as the first member
        messages: [],
    });

    await group.save({ session });

    res.status(201);
    return { message: 'Group created successfully', group };
}));

// Get all groups
const getGroups = errorHandler(withTransaction(async (req, res) => {
    const groups = await models.Group.aggregate([
        {
            $project: {
                name: 1, // include the group name in the result
                description: 1, // include the group description in the result
                numberOfMembers: { $size: "$members" } // add a field to count the number of members
            }
        }
    ]);
    return { groups };
}));

// Get a group by ID
const getGroup = errorHandler(withTransaction(async (req, res) => {
    const group = await models.Group.findById(req.params.id).populate({
        path: 'members',
        select: 'email -_id'  // Include email and exclude the _id field
    });

    if (!group) {
        throw new HttpError(404, 'Group not found');
    } else {
        return { group };
    }
}));

// Update a group by ID
const updateGroup = errorHandler(withTransaction(async (req, res, session) => {
    const group = await models.Group.findById(req.params.id)

    if (!group) {
        throw new HttpError(404, 'Group not found');
    } else {
        // Check if the user is a member of the group (you may need to implement this check)
        if (group.members.includes(req.user.id)) {
            group.name = req.body.name || group.name;
            group.description = req.body.description || group.description;
            await group.save({ session });

            // Remove the members property from the response
            const updatedGroup = group.toObject(); // Convert the document to a plain JavaScript object
            delete updatedGroup.members; // Delete the members property

            return { message: 'Group updated successfully', group: updatedGroup }
        } else {
            throw new HttpError(403, 'You are not a member of this group');
        }
    }
}));

// Delete a group by ID
const deleteGroup = errorHandler(withTransaction(async (req, res, session) => {
    const group = await models.Group.findById(req.params.id);
    if (!group) {
        throw new HttpError(404, 'Group not found');
    } else {
        // Check if the user is the creator of the group (you may need to implement this check)
        if (group.members[0].toString() === req.user.id) {
            await group.deleteOne({ session });
            res.status(204);
            return { message: 'Group Delete successfully!' }
        } else {
            throw new HttpError(403, 'You are not the creator of this group');
        }
    }
}));

// Join a group by ID
const joinGroup = errorHandler(withTransaction(async (req, res, session) => {
    const group = await models.Group.findById(req.params.id);
    const user = await models.User.findById(req.user.id);

    if (!group) {
        throw new HttpError(404, 'Group not found');
    }

    if (!user) {
        throw new HttpError(404, 'User not found');
    }

    // Check if the user is already a member of the group
    if (!user.groups.includes(group._id) && !group.members.includes(req.user.id)) {
        group.members.push(req.user.id);
        user.groups.push(group._id);

        await Promise.all([group.save({ session }), user.save({ session })]);
        return { message: 'Joined the group' }
    } else {
        throw new HttpError(400, 'You are already a member of this group');
    }

}));

const getGroupMessages = errorHandler(withTransaction(async (req, res) => {
    const { groupId } = req.params;
    const userId = req.user.id;

    // First, find the group without populating the user details
    const group = await models.Group.findById(groupId);

    if (!group) {
        throw new HttpError(404, 'Group not found');
    }

    // Check if the user is a member of the group
    if (!group.members.map(member => member.toString()).includes(userId)) {
        throw new HttpError(403, 'Not authorized to view messages');
    }

    // Now that we've verified the user is a member, we can safely populate the messages
    // including only the email of the user for each message
    await group.populate({
        path: 'messages.user',
        select: 'email -_id'
    });

    return { messages: group.messages };
}));


export default {
    createGroup,
    getGroups,
    getGroup,
    updateGroup,
    deleteGroup,
    joinGroup,
    getGroupMessages
}